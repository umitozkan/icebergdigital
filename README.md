## Iceberg Digital Case

This project was developed for iceberg digital.

### Installation 

Env file copy

```
cp .env.example .env
```

Installation laravel & libraries

```
composer install
```

Create database tables and insert dummy data

```
php artisan migrate:fresh --seed
```
JWT key generate
```
php artisan jwt:secret
```
Insert  `.env`  file 

```
GOOGLE_API_KEY=
```

App key generate

```
php artisan key:generate
```

### Postman Links 

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/3035267-959ab8db-2b39-4932-a517-8cae9f89ddc5?action=collection%2Ffork&collection-url=entityId%3D3035267-959ab8db-2b39-4932-a517-8cae9f89ddc5%26entityType%3Dcollection%26workspaceId%3D1538d200-58f3-4033-93cd-861bc084d5c2#?env%5BLocal%5D=W3sia2V5IjoiYmFzZV91cmwiLCJ2YWx1ZSI6Imh0dHA6Ly9pY2ViZXJnLmxvY2FsL2FwaSIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoiSldUX1RPS0VOIiwidmFsdWUiOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuZXlKcGMzTWlPaUpvZEhSd09sd3ZYQzlwWTJWaVpYSm5MbXh2WTJGc1hDOXdkV0pzYVdOY0wyRndhVnd2Ykc5bmFXNGlMQ0pwWVhRaU9qRTJNekkyTmpVMU9ERXNJbVY0Y0NJNk1UWXpNalkyT1RFNE1Td2libUptSWpveE5qTXlOalkxTlRneExDSnFkR2tpT2lKTlNGZHpRakExYmxGMVVFNDFNbEZ1SWl3aWMzVmlJam94TENKd2NuWWlPaUl5TTJKa05XTTRPVFE1WmpZd01HRmtZak01WlRjd01XTTBNREE0TnpKa1lqZGhOVGszTm1ZM0luMC4tZ281c0R4UEpLbnRiNmtwdEFZR3hrajJRdHVVY1g5NTRUSGMtRTUyUzJVIiwiZW5hYmxlZCI6dHJ1ZX1d)


## Documentations

[Postman Docs](https://documenter.getpostman.com/view/3035267/UUxxhUTv)


### Notes

From my point of view, things should be done are like the following:
- Some methods need to be split to improve code quality and readability.
- Unit tests need to be written.
- Docker installation can be done.
- Notifications can be sent by e-mail close to the reservation time of the users.


   <em>If my time wasn’t limited, i would like to do the things mentioned above </em>
