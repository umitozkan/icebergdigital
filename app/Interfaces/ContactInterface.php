<?php

namespace App\Interfaces;


use Illuminate\Http\Request;

interface ContactInterface
{

    /**
     * @return mixed
     */
    public function getAllContacts();


    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);


    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     */
    public function requestContact(Request $request, $id = null);


    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
