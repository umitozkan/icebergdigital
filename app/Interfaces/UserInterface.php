<?php

namespace App\Interfaces;


use Illuminate\Http\Request;

interface UserInterface
{

    /**
     * Get All Users
     * @return mixed
     * @method @GET api/users/
     */
    public function getAllUsers();


    /**
     * @param $id
     * @return mixed
     */
    public function getUserById($id);


    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     */
    public function requestUser(Request $request, $id = null);


    /**
     * @param $id
     * @return mixed
     */
    public function deleteUser($id);
}
