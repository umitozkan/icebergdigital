<?php

namespace App\Interfaces;


use Illuminate\Http\Request;

interface AppointmentsInterface
{

    /**
     * @return mixed
     */
    public function getAllAppointments();


    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);


    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     */
    public function requestAppointment(Request $request, $id = null);


    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);
}
