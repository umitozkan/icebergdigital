<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {

        $this->app->bind(
            'App\Interfaces\UserInterface',
            'App\Repositories\UserRepository'
        );

        $this->app->bind(
            'App\Interfaces\ContactInterface',
            'App\Repositories\ContactRepository'
        );
        $this->app->bind(
            'App\Interfaces\AppointmentsInterface',
            'App\Repositories\AppointmentsRepository'
        );
    }
}
