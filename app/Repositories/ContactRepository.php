<?php

namespace App\Repositories;

use App\Interfaces\ContactInterface;
use App\Models\Contacts;
use App\Traits\ResponseAPI;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ContactRepository implements ContactInterface
{
    // Use ResponseAPI Trait in this repository
    use ResponseAPI;

    /**
     * @return JsonResponse|mixed
     */
    public function getAllContacts()
    {
        try {
            $contacts = Contacts::all();
            return $this->success("All Contacts", $contacts);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getById($id): JsonResponse
    {
        try {
            $contact = Contacts::find($id);

            if (!$contact) return $this->error("No contact with ID $id", 404);

            return $this->success("Contact Detail", $contact);
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @return JsonResponse
     */
    public function requestContact(Request $request, $id = null): JsonResponse
    {
        DB::beginTransaction();
        try {
            $request = json_decode($request->getContent());

            $rules = [
                'name' => 'required|max:50',
                'surname' => 'required|max:50',
                'email' => request()->route('id')
                    ? 'required|email|max:255|unique:contacts,email,' . request()->route('id')
                    : 'required|email|max:255|unique:contacts,email',
                'phone' => request()->route('id') ? 'nullable' : 'required|max:50'
            ];

            $validator = Validator::make((array)$request, $rules);

            if ($validator->fails()) {
                return $this->error("VALIDATE_ERROR", $validator->errors(), 400);
            }

            $contact = $id ? Contacts::find($id) : new Contacts();

            if ($id && !$contact) return $this->error("No contact with ID $id", 404);

            $contact->name = $request->name;
            $contact->surname = $request->surname;
            $contact->phone = $request->phone;
            $contact->email = preg_replace('/\s+/', '', strtolower($request->email));

            $contact->save();

            DB::commit();
            return $this->success(
                $id ? "Contact updated"
                    : "Contact created",
                $contact, $id ? 200 : 201);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse|mixed
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $contact = Contacts::find($id);

            if (!$contact) return $this->error("NOT_FOUND", "$id CONTACT NOT FOUND", 404);

            $contact->delete();

            DB::commit();
            return $this->success("Contact deleted", $contact);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
}
