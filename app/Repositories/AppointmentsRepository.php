<?php

namespace App\Repositories;

use App\Interfaces\AppointmentsInterface;
use App\Models\Appointments;
use App\Models\Contacts;
use App\Traits\ResponseAPI;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AppointmentsRepository implements AppointmentsInterface
{
    // Use ResponseAPI Trait in this repository
    use ResponseAPI;

    /**
     * @return JsonResponse|mixed
     */
    public function getAllAppointments()
    {
        try {
            $appointments = Appointments::all();
            return $this->success("All Appointmets", $appointments);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getById($id): JsonResponse
    {
        try {
            $appointments = Appointments::find($id);

            if (!$appointments) return $this->error("No appointments with ID $id", 404);

            return $this->success("Appointments Detail", $appointments);
        } catch (Exception $exception) {
            return $this->error($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @return JsonResponse
     */
    public function requestAppointment(Request $request, $id = null): JsonResponse
    {
        DB::beginTransaction();

        try {
            $request = json_decode($request->getContent());

            $rules = [
                'address' => 'required',
                'surname' => 'required|max:50',
                'date' => 'date_format:Y-m-d H:i',
                'name' => 'required|max:50',
                'phone' => 'required',
                'email' => 'required|email',
            ];

            $validator = Validator::make((array)$request, $rules);

            if ($validator->fails()) {
                return $this->error("VALIDATE_ERROR", $validator->errors(), 400);
            }

            $checkAppointments = Appointments::where('user_id', auth()->user()->id);
            $checkContact = Contacts::where('email', $request->email)->first();


            if ($checkContact) {
                $contactId = $checkContact->id;
            }

            $contact = new Contacts();
            $contact->name = $request->name;
            $contact->surname = $request->surname;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            if ($contact->save()) {
                $contactId = $contact->id;
            }

            if ($checkAppointments->get()) {
                foreach ($checkAppointments as $checkAppointment) {
                    if ($checkAppointment->available_time >= strtotime($request->date)) {
                        return $this->error('NOT_AVAILABLE', "This date is not available.", 400);
                    }
                }
            }

            $outgoing = getDistance(auth()->user()->zip_code, $request->address);
            $ingoing = getDistance($request->address, auth()->user()->zip_code);

            $estimateTime = Carbon::parse($request->date)->subMinute(ceil($outgoing->duration));
            $availableTime = Carbon::parse($request->date)->addMinute(ceil($ingoing->duration) + 60);

            $appointment = $id ? Appointments::find($id) : new Appointments();

            if ($id && !$appointment) return $this->error("No Appointment with ID $id", 404);

            $appointment->address = $request->address;
            $appointment->date = $request->date;
            $appointment->distance = ceil($outgoing->distance);
            $appointment->duration = ceil($outgoing->duration);
            $appointment->available_time = $availableTime;
            $appointment->estimate_time = $estimateTime;
            $appointment->user_id = auth()->user()->id;
            $appointment->contact_id = $contactId;
            $appointment->save();


            DB::commit();
            return $this->success(
                $id ? "Appointment updated"
                    : "Appointment created",
                $appointment, $id ? 200 : 201);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse|mixed
     */
    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $appointment = Appointments::find($id);

            if (!$appointment) return $this->error("NOT_FOUND", "$id Appointment NOT FOUND", 404);

            $appointment->delete();

            DB::commit();
            return $this->success("Appointment deleted", $appointment);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
}
