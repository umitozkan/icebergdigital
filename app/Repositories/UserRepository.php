<?php

namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\Traits\ResponseAPI;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserInterface
{
    // Use ResponseAPI Trait in this repository
    use ResponseAPI;

    /**
     * @return JsonResponse|mixed
     */
    public function getAllUsers()
    {
        try {
            $users = User::all();
            return $this->success("All Users", $users);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getUserById($id): JsonResponse
    {
        try {
            $user = User::find($id);

            // Check the user
            if (!$user) return $this->error("No user with ID $id", 404);

            return $this->success("User Detail", $user);
        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param null $id
     * @return JsonResponse
     */
    public function requestUser(Request $request, $id = null): JsonResponse
    {
        DB::beginTransaction();

        try {
            $request = json_decode($request->getContent());

            $rules = [
                'name' => 'required|max:50',
                'email' => request()->route('id')
                    ? 'required|email|max:255|unique:users,email,' . request()->route('id')
                    : 'required|email|max:255|unique:users,email',
                'password' => request()->route('id') ? 'nullable' : 'required|max:50'
            ];

            $validator = Validator::make((array)$request, $rules);

            if ($validator->fails()) {
                return $this->error("VALIDATE_ERROR", $validator->errors(), 400);
            }

            $user = $id ? User::find($id) : new User;

            // Check the user
            if ($id && !$user) return $this->error("No user with ID $id", 404);

            $user->name = $request->name;
            $user->email = preg_replace('/\s+/', '', strtolower($request->email));
            $user->status = 1;
            // I dont wanna to update the password,
            // Password must be fill only when creating a new user.
            if (!$id) $user->password = Hash::make($request->password);

            // Save the user
            $user->save();

            DB::commit();
            return $this->success(
                $id ? "User updated"
                    : "User created",
                $user, $id ? 200 : 201);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return JsonResponse|mixed
     */
    public function deleteUser($id)
    {
        DB::beginTransaction();
        try {
            $user = User::find($id);

            // Check the user
            if (!$user) return $this->error("NOT_FOUND", "$id USER NOT FOUND", 404);

            // Delete the user
            $user->delete();

            DB::commit();
            return $this->success("User deleted", $user);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
}
