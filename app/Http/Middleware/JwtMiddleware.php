<?php

namespace App\Http\Middleware;

use App\Traits\ResponseAPI;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    use ResponseAPI;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {

            $user = JWTAuth::parseToken()->authenticate();

            if( !$user ) throw new Exception('User Not Found');

        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException) {
                return $this->error("TOKEN_IS_INVALID","Token is invalid", 401);
            } else if ($e instanceof TokenExpiredException) {
                return $this->error("TOKEN_EXPIRED","Token is Expired", 401);
            } else {
                return $this->error("TOKEN_NOT_FOUND","Authorization Token not found", 401);
            }
        }
        return $next($request);
    }
}
