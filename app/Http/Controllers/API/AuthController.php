<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ResponseAPI;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    use ResponseAPI;

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function register(Request $request, User $user): JsonResponse
    {

        try {
            $request = json_decode($request->getContent());

            $rules = [
                'name' => 'required|string',
                'email' => 'email|required|string',
                'password' => 'required',
            ];

            $validator = Validator::make((array)$request, $rules);

            if ($validator->fails()) {
                return $this->error("VALIDATE_ERROR", $validator->errors(), 400);
            }


            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->zip_code = "cm27pj";
            $user->phone = $request->phone;
            $user->save();


            $payloads = [
                "phone" => $user->phone,
                "zip_code" => $user->zip_code,
            ];
            $token = auth()->claims($payloads)->login($user);

            return $this->respondWithToken($token);


        } catch (QueryException $exception) {
            $errorCode = $exception->errorInfo[1];
            if ($errorCode == '1062') {
                return $this->error('ALREADY_EXIST', $exception->getMessage(), 409);
            } else {
                return $this->error("QUERY_ERROR", $exception->getMessage());
            }
        } catch (ModelNotFoundException $exception) {
            $modelArr = explode('\\', $exception->getModel());
            return $this->error("NOT_FOUND", $exception->getMessage(), 404);
        } catch (Exception $exception) {
            return $this->error('INTERNAL_SERVER_ERROR', $exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $request = json_decode($request->getContent());

            $credentials = ['email' => $request->email, 'password' => $request->password];
            $rules = [
                'email' => 'required|email',
                'password' => 'required',
            ];

            $validator = Validator::make((array)$request, $rules);

            if ($validator->fails()) {
                return $this->error("VALIDATE_ERROR", $validator->errors(), 400);
            }
            $token = auth()->attempt($credentials);
            if (!$token) {
                return $this->error("AUTH_ERROR", "You have entered an invalid username or password.", 401);
            }

            return $this->respondWithToken($token);
        } catch (ModelNotFoundException $exception) {
            return $this->error("NOT_FOUND", $exception->getMessage(), 404);
        } catch (Exception $exception) {
            return $this->error("INTERNAL_SERVER_ERROR", $exception->getMessage());
        }

    }

    /**
     * @param $token
     * @return JsonResponse
     */
    protected function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory(User::class)->getTTL() * 60
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json("Logout successfully.");
    }

}
