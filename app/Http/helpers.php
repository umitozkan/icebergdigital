<?php

function getDistance($addressFrom, $addressTo)
{
    //Change address format
    $addressFrom = str_replace(' ', '+', $addressFrom);
    $addressTo = str_replace(' ', '+', $addressTo);

    $geocodeTo = file_get_contents('https://maps.google.com/maps/api/distancematrix/json?key='.env('GOOGLE_API_KEY').'&units=metric&origins=' . $addressFrom . '&destinations=' . $addressTo);
    $outputTo = json_decode($geocodeTo);
    $response = $outputTo->rows[0]->elements[0];
    return (object) [
        "distance" => $response->distance->value / 1000, //meters
        "duration" => $response->duration->value / 60 //minute
    ];

}
