<?php

use App\Http\Controllers\API\AppointmentsController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ContactController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);
Route::get('logout', [AuthController::class, 'logout']);




Route::group(["middleware" => 'jwt.verify'], function () {

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/', [UserController::class, 'store']);
        Route::put('/{id}', [UserController::class, 'update']);
        Route::get('/{id}', [UserController::class, 'show']);
    });

    Route::group(['prefix' => 'appointments'], function () {
        Route::get('/', [AppointmentsController::class, 'index']);
        Route::post('/', [AppointmentsController::class, 'store']);
        Route::put('/{id}', [AppointmentsController::class, 'update']);
        Route::get('/{id}', [AppointmentsController::class, 'show']);
    });

    Route::group(['prefix' => 'contacts'], function () {
        Route::get('/', [ContactController::class, 'index']);
        Route::post('/', [ContactController::class, 'store']);
        Route::put('/{id}', [ContactController::class, 'update']);
        Route::get('/{id}', [ContactController::class, 'show']);
    });
});
